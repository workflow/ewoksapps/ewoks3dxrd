import os
import shutil
from pathlib import Path
import h5py

from ImageD11 import frelon_peaksearch
import numpy as np

from ImageD11.columnfile import colfile_from_dict
from ImageD11.blobcorrector import correct_cf_with_spline  # correct_cf_with_dxdyfiles,
from ImageD11 import columnfile as PeakColumnFile
from ImageD11.unitcell import unitcell
from ImageD11.peakselect import (
    filter_peaks_by_phase,
    sorted_peak_intensity_mask_and_cumsum,
)
from ImageD11.indexing import indexer_from_colfile_and_ucell
from ImageD11 import indexing
from ImageD11.indexing import do_index
from ImageD11 import grain
from ewoks3dxrd.tasks.utils import save_grains_to_nexus
from ImageD11 import refinegrains

prefix_path = os.environ["HOME"] + "/3dxrd_script_result"
Path(prefix_path).mkdir(parents=True, exist_ok=True)

if __name__ == "__main__":

    # init folder file config task
    masterfile = (
        "/mnt/id03_3dxrd/expt/RAW_DATA/FeAu_0p5_tR/FeAu_0p5_tR_ff1/FeAu_0p5_tR_ff1.h5"
    )
    detector = "frelon3"
    omegamotor = "diffrz"
    dtymotor = "diffty"
    scan_number = "1"
    with h5py.File(str(masterfile), "r") as hin:
        omega_angles = hin[str(scan_number) + ".1"]["measurement"].get(omegamotor, None)
        omega_array = omega_angles[()]

    # segment scan task
    segmenter_settings = {
        "bgfile": None,
        "maskfile": "/mnt/id03_3dxrd/expt/PROCESSED_DATA/mask.edf",
        "threshold": 70,
        "smoothsigma": 1.0,
        "bgc": 0.9,
        "minpx": 3,
        "m_offset_thresh": 100,
        "m_ratio_thresh": 150,
    }

    all_frames_2d_peaks_list = frelon_peaksearch.segment_master_file(
        masterfile,
        str(scan_number) + ".1" + "/measurement/" + detector,
        omega_array,
        segmenter_settings,
    )
    peaks_2d_dict, num_peaks = frelon_peaksearch.peaks_list_to_dict(
        all_frames_2d_peaks_list
    )
    # 3d merge from 2d peaks dict
    peak_3d_dict = frelon_peaksearch.do3dmerge(peaks_2d_dict, num_peaks, omega_array)

    # Detector Spatial Correction Task
    peak_3d_dict["spot3d_id"] = np.arange(len(peak_3d_dict["s_raw"]))
    seg_3d_col_file = prefix_path + "/segmented_3d_columnfile.h5"
    columnfile_3d = colfile_from_dict(peak_3d_dict)
    spline_file = "/mnt/id03_3dxrd/expt/PROCESSED_DATA/frelon36.spline"
    columnfile_3d = correct_cf_with_spline(columnfile_3d, spline_file)
    PeakColumnFile.colfile_to_hdf(columnfile_3d, seg_3d_col_file)

    # Geometry Transformation Task
    os.makedirs(prefix_path + "/par_folder", exist_ok=True)
    geo_file_path = "/mnt/id03_3dxrd/expt/PROCESSED_DATA/geometry_tdxrd.par"
    new_geometry_par_file = prefix_path + "/par_folder/geometry_tdxrd.par"
    shutil.copy2(geo_file_path, new_geometry_par_file)
    columnfile_3d.parameters.loadparameters(filename=new_geometry_par_file)
    columnfile_3d.updateGeometry()
    PeakColumnFile.colfile_to_hdf(columnfile_3d, seg_3d_col_file)

    # Phase Determination
    phase_file = "/mnt/id03_3dxrd/expt/PROCESSED_DATA/Fe.par"
    shutil.copy2(phase_file, prefix_path + "/par_folder/Fe.par")
    lattice_parameters = (2.8694, 2.8694, 2.8694, 90.0, 90.0, 90.0)
    symmetry = 229
    ucell = unitcell(lattice_parameters=lattice_parameters, symmetry=symmetry)
    ucell.makerings(limit=columnfile_3d.ds.max())
    eta = columnfile_3d.eta
    ds = columnfile_3d.ds

    from matplotlib import pyplot as plt

    fig, ax = plt.subplots(figsize=(16, 9), layout="constrained")
    ax.scatter(ds, eta, s=1)
    ax.plot(
        ucell.ringds,
        [
            0,
        ]
        * len(ucell.ringds),
        "|",
        ms=90,
        c="red",
    )

    # Ring Filter by phase Task
    cf_strong_frac = 0.9837
    cf_strong_dsmax = 1.01
    cf_strong_dstol = 0.01
    cf = columnfile_3d.copyrows(columnfile_3d.ds <= cf_strong_dsmax)
    filtered_peaks_cf_fe = filter_peaks_by_phase(
        cf=cf, dstol=cf_strong_dstol, dsmax=cf_strong_dsmax, cell=ucell
    )
    print(f"Got {filtered_peaks_cf_fe.nrows} strong peaks for indexing")
    filtered_peaks_cf_fe_path = prefix_path + "/Fe_ff1_3d_peaks_strong.flt"
    filtered_peaks_cf_fe.writefile(filtered_peaks_cf_fe_path)

    # filter peaks by intensity Task
    mask, cums = sorted_peak_intensity_mask_and_cumsum(
        colf=filtered_peaks_cf_fe, frac=cf_strong_frac
    )
    filtered_peaks_cf_fe.filter(mask)

    print(f"Got {filtered_peaks_cf_fe.nrows} strong peaks for indexing")
    cf_strong_path = prefix_path + "/strong_3d_peaks_fe_ph_intensity_filtered.flt"
    filtered_peaks_cf_fe.writefile(cf_strong_path)

    # storing all peaks for makemap
    cf_strong_allrings_frac = cf_strong_frac
    cf_strong_allrings_dstol = cf_strong_dstol

    all_peaks_cf_fe = filter_peaks_by_phase(
        cf=columnfile_3d,
        dstol=cf_strong_dstol,
        dsmax=columnfile_3d.ds.max(),
        cell=ucell,
    )
    mask, cums = sorted_peak_intensity_mask_and_cumsum(
        colf=all_peaks_cf_fe, frac=cf_strong_allrings_frac
    )
    all_peaks_cf_fe.filter(mask)
    print(f"Got {all_peaks_cf_fe.nrows} strong peaks for makemap")
    cf_strong_allrings_path = prefix_path + "/all_3d_peaks_fe_ph_intensity_filtered.flt"
    all_peaks_cf_fe.writefile(cf_strong_allrings_path)

    # plot that shows to user for him have some choise on how to choose rings for indexing
    indexer = indexer_from_colfile_and_ucell(
        colfile=filtered_peaks_cf_fe, ucell=ucell, wavelength=0.28457041
    )
    print(f"Indexing {filtered_peaks_cf_fe.nrows} peaks")

    # USER: set a tolerance in d-space (for assigning peaks to powder rings)
    indexer_ds_tol = 0.05
    indexer.ds_tol = indexer_ds_tol
    indexing.loglevel = 1
    indexer.assigntorings()
    indexing.loglevel = 3

    ds = all_peaks_cf_fe.ds
    eta = all_peaks_cf_fe.eta
    color = indexer.ra
    cake_rings = ucell.ringds

    # selecting rings (ring selection task)
    rings_for_gen = [0, 1]
    rings_for_scoring = [0, 1, 2, 3]
    hkl_tols_seq = [0.01, 0.02, 0.03, 0.04]
    fracs = [0.9, 0.75]
    cosine_tol = np.cos(np.radians(90 - 0.25))
    max_grains = 1000

    # do indexing Task
    grains, indexer = do_index(
        cf=filtered_peaks_cf_fe,
        dstol=indexer.ds_tol,
        forgen=rings_for_gen,
        foridx=rings_for_scoring,
        hkl_tols=hkl_tols_seq,
        fracs=fracs,
        cosine_tol=cosine_tol,
        max_grains=max_grains,
        unitcell=ucell,
        wavelength=0.28457041,
    )
    # set grain GIDs (useful if we ever delete a grain)
    for i, g in enumerate(grains):
        g.gid = i
        g.translation = np.array([0.0, 0.0, 0.0])
    grain_ubi_file_path = prefix_path + "/Fe_grains.ubi"
    grain.write_grain_file(grain_ubi_file_path, grains)
    grain_nexus_file_path = prefix_path + "/Fe_grains_nexus.h5"
    save_grains_to_nexus(
        nexus_filename=grain_nexus_file_path,
        geometry_par_path=str(new_geometry_par_file),
        lattice_par_file=phase_file,
        lattice_name="Fe",
        grains=grains,
    )

    # do grain mapping Task
    merged_par_file = prefix_path + "/geo_lattice.par"
    with open(prefix_path + "/par_folder/Fe.par", "r") as fe_file:
        fe_content = fe_file.read().strip()
    with open(prefix_path + "/par_folder/geometry_tdxrd.par", "r") as geo_file:
        geo_lines = [line for line in geo_file]
    merged_content = f"{fe_content}\n" + "".join(geo_lines)
    with open(merged_par_file, "w") as out_file:
        out_file.write(merged_content)
    intensity_tth_range = (0, 180)
    symmetry = "cubic"
    makemap_hkl_tol_seq = [0.05, 0.025, 0.01]
    omegas_sorted = np.sort(omega_array)
    omega_step = np.round(np.diff(omegas_sorted).mean(), 3)
    omega_slop = omega_step / 2
    tmp_grain_map_path = prefix_path + "/tmp_Fe_grains.map"
    shutil.copy2(grain_ubi_file_path, tmp_grain_map_path)
    unindexed_flt_path = (
        prefix_path + "/all_3d_peaks_fe_ph_intensity_filtered.flt.unindexed"
    )

    for i, tol in enumerate(makemap_hkl_tol_seq):
        refine_grain = refinegrains.refinegrains(
            intensity_tth_range=intensity_tth_range, OmSlop=omega_slop
        )
        refine_grain.loadparameters(str(merged_par_file))
        refine_grain.loadfiltered(str(cf_strong_allrings_path))
        refine_grain.readubis(str(tmp_grain_map_path))
        refine_grain.makeuniq(symmetry)
        refine_grain.tolerance = float(makemap_hkl_tol_seq[i])
        refine_grain.generate_grains()
        refine_grain.refinepositions()
        refine_grain.savegrains(str(tmp_grain_map_path), sort_npks=True)
        refine_grain.scandata[str(cf_strong_allrings_path)].writefile(
            cf_strong_allrings_path + ".new"
        )
        refine_grain.assignlabels()
        col = refine_grain.scandata[cf_strong_allrings_path].copy()
        col.filter(col.labels < -0.5)
        col.writefile(unindexed_flt_path)

    grains2 = grain.read_grain_file(tmp_grain_map_path)
    grains2 = [grain for grain in grains2 if "no peaks" not in grain.intensity_info]
    print(len(grains2))  # from James notebook it should be 58
    absolute_minpks = 120
    grains_filtered = [
        grain for grain in grains2 if float(grain.npks) > absolute_minpks
    ]
    print(len(grains_filtered))  # from James notebook it should be 52
    map_path = prefix_path + "/grains_filtered.map"
    grain.write_grain_file(map_path, grains_filtered)
    final_unindexed_flt_path = prefix_path + "/unindexed_Fe_3d_peaks.flt"
    final_new_flt_path = prefix_path + "/new_3d_peaks.flt"

    refine_grain = refinegrains.refinegrains(
        intensity_tth_range=intensity_tth_range, OmSlop=omega_slop
    )
    refine_grain.loadparameters(merged_par_file)
    refine_grain.loadfiltered(cf_strong_path)
    refine_grain.readubis(map_path)
    refine_grain.makeuniq(symmetry)
    refine_grain.tolerance = float(makemap_hkl_tol_seq[-1])
    refine_grain.generate_grains()
    refine_grain.refinepositions()
    refine_grain.savegrains(map_path, sort_npks=True)
    col = refine_grain.scandata[cf_strong_path].writefile(final_new_flt_path)
    refine_grain.assignlabels()
    col = refine_grain.scandata[cf_strong_path].copy()
    col.filter(col.labels < -0.5)
    col.writefile(final_unindexed_flt_path)
    grains_filtered = grain.read_grain_file(map_path)
    print(len(grains_filtered))  # from James notebook it should be 52
    if os.path.exists(prefix_path + "/Fe_ImageD11_grains.h5"):
        os.remove(prefix_path + "/Fe_ImageD11_grains.h5")
    grain.write_grain_file_h5(
        prefix_path + "/Fe_ImageD11_grains.h5", grains_filtered, group_name="Fe"
    )
    print("FINISHED!")
