# ewoks3dxrd {{version}}

*ewoks3dxrd* is a project to create Ewoks 3DXRD workflows based on [ImageD11](https://github.com/FABLE-3DXRD/ImageD11).

```{warning}
This project is actively worked on and may not be ready for use yet!
```


## Documentation

```{toctree}
:maxdepth: 2

spec
```
