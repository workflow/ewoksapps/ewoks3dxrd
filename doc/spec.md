# Workflow specifications

## Task graph

```{mermaid}
graph TD
    raw([Raw data]) ---> 
    Segmenting --->
    2ds(["`Segmented 2d peaks
    _s_raw_, _f_raw_, _omega_raw_`"]) ---> 
    Merging --->
    3dp(["`Segmented 3D peaks
    Same columns but merging
    rows`"]) --->
    spc[Spatial correction] --->
    3dpsc(["`Segmented 3D peaks
    Adds _fc_, _sc_ columns`"]) --->
    geoc[Geometry calculation] --->
    3dscg(["`Segmented 3d peaks
    Adds _tth_, _eta_, _xl_, _yl_, 
    _zl_, _gx_, _gy_, _gz_, _ds_`"]) --->
    phasef[Filter by phase/lattice] --->
    3dspf(["`Segmented 3d peaks 
    filtered by phase`"]) --->
    intensityf[Filter by intensity] --->
    3dsipf(["`Segmented 3d peaks 
    filtered by phase 
    and intensity`"]) --->
    indexing["`Indexing 
    (and UB matrix calculation)`"] --->
    ...
```

## Tasks API

```{ewokstasks} ewoks3dxrd.tasks.*
    :task_type: class
```