from pathlib import Path
import pytest

from ewokscore import execute_graph
from ewoksorange.tests.utils import execute_task

from ewoks3dxrd.tasks.segmentFrame import SegmentFrame


def test_segmentation_workflow(inp_config):

    # if you change this config setting,
    # assertion statement assert len(found_peaks[0]) == 76 will fail
    algo_par = {
        "threshold": 70,
        "smooth_sigma": 1.0,
        "bgc": 0.9,
        "min_px": 3,
        "offset_threshold": 100,
        "ratio_threshold": 150,
    }

    node_1 = {
        "id": "init",
        "task_type": "class",
        "task_identifier": "ewoks3dxrd.tasks.initFolderFileConfig.InitFolderFileConfig",
    }
    node_2 = {
        "id": "segFrame",
        "task_type": "class",
        "task_identifier": "ewoks3dxrd.tasks.segmentFrame.SegmentFrame",
    }
    link = {
        "source": "init",
        "target": "segFrame",
        "data_mapping": [
            {"source_output": "config_dict", "target_input": "folder_file_config"}
        ],
    }
    inputs = [
        {"name": "config_dict", "value": inp_config, "id": "init"},
        {"name": "segmenter_config", "value": algo_par, "id": "segFrame"},
    ]

    workflow = {
        "graph": {"id": "segment_frame_wf"},
        "nodes": [node_1, node_2],
        "links": [link],
    }
    outputs = execute_graph(
        workflow,
        inputs=inputs,
    )
    assert outputs is not None

    segmenter_config = outputs["merged_config"]
    assert segmenter_config is not None
    assert segmenter_config["folder_file_config"]["bg_file"] is None
    assert (
        segmenter_config["folder_file_config"]["mask_file"]
        == "/mnt/id03_3dxrd/expt/PROCESSED_DATA/mask.edf"
    )
    assert Path(
        segmenter_config["folder_file_config"]["stateful_imageD11_file"]
    ).exists()
    assert (
        Path(segmenter_config["folder_file_config"]["stateful_imageD11_file"]).name
        == "FeAu_0p5_tR_ff1_dataset.h5"
    )

    par = segmenter_config["segmenter_parameter"]
    assert par["threshold"] == 70
    assert par["smooth_sigma"] == 1.0
    assert par["bgc"] == 0.9
    assert par["min_px"] == 3
    assert par["offset_threshold"] == 100
    assert par["ratio_threshold"] == 150

    raw_image = outputs["raw_image"]
    bg_corrected_image = outputs["bg_corrected_image"]
    found_peaks = outputs["found_peaks"]
    assert raw_image is not None
    assert bg_corrected_image is not None
    assert found_peaks is not None
    assert len(found_peaks) == 2
    assert len(found_peaks[0]) == 76


def test_wrong_threshold(inp_config):
    segmenter_config = {
        "threshold": "not_a_valid_threshold",
        "smooth_sigma": 1.0,
        "bgc": 0.9,
        "min_px": 3,
        "offset_threshold": 100,
        "ratio_threshold": 150,
    }

    with pytest.raises(RuntimeError) as exc_info:
        execute_task(
            SegmentFrame,
            inputs={
                "folder_file_config": inp_config,
                "segmenter_config": segmenter_config,
            },
        )
    assert "Input should be a valid integer" in str(exc_info.value.__cause__)
