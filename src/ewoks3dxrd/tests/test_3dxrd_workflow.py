from ewokscore import execute_graph
from pathlib import Path
import numpy as np
import h5py
from ImageD11 import columnfile as PeakColumnFile


def test_segmentation_3dpeaks_workflow(inp_config):
    # if you change this config setting or default inp_config,
    # assertion statement assert len(found_peaks[0]) == 76 and
    # and len(peaks_position[0]) == 31682 and
    # assert cf.nrows == 7662 will fail
    algo_par = {
        "threshold": 70,
        "smooth_sigma": 1.0,
        "bgc": 0.9,
        "min_px": 3,
        "offset_threshold": 100,
        "ratio_threshold": 150,
    }

    node_1 = {
        "id": "init",
        "task_type": "class",
        "task_identifier": "ewoks3dxrd.tasks.initFolderFileConfig.InitFolderFileConfig",
    }
    node_2 = {
        "id": "segFrame",
        "task_type": "class",
        "task_identifier": "ewoks3dxrd.tasks.segmentFrame.SegmentFrame",
    }
    node_3 = {
        "id": "segScan",
        "task_type": "class",
        "task_identifier": "ewoks3dxrd.tasks.segmentScan.SegmentScan",
    }
    node_4 = {
        "id": "DetCor",
        "task_type": "class",
        "task_identifier": "ewoks3dxrd.tasks.DetectorSpatialCorrection.DetectorSpatialCorrection",
    }
    node_5 = {
        "id": "GeoUp",
        "task_type": "class",
        "task_identifier": "ewoks3dxrd.tasks.geometryTransformation.GeometryTransformation",
    }
    node_6 = {
        "id": "LatFilt",
        "task_type": "class",
        "task_identifier": "ewoks3dxrd.tasks.ringFilterbyLattice.RingFilterLattice",
    }
    node_7 = {
        "id": "IntFilt",
        "task_type": "class",
        "task_identifier": "ewoks3dxrd.tasks.PeaksFilterByIntensity.PeaksFilterByIntensity",
    }
    node_8 = {
        "id": "Indexer",
        "task_type": "class",
        "task_identifier": "ewoks3dxrd.tasks.UBIndexing.UBIndexing",
    }

    link_1 = {
        "source": "init",
        "target": "segFrame",
        "data_mapping": [
            {"source_output": "config_dict", "target_input": "folder_file_config"}
        ],
    }
    link_2 = {
        "source": "segFrame",
        "target": "segScan",
        "data_mapping": [
            {"source_output": "merged_config", "target_input": "segmenter_config"}
        ],
    }
    link_3 = {
        "source": "segScan",
        "target": "DetCor",
        "data_mapping": [
            {
                "source_output": "segmented_3d_peaks_file",
                "target_input": "segmented_3d_peaks_file",
            }
        ],
    }
    link_4 = {
        "source": "DetCor",
        "target": "GeoUp",
        "data_mapping": [
            {
                "source_output": "detector_spatial_corrected_3d_peaks_file",
                "target_input": "spatial_corrected_peaks_3d_file",
            }
        ],
    }
    link_5 = {
        "source": "GeoUp",
        "target": "LatFilt",
        "data_mapping": [
            {
                "source_output": "geometry_updated_3d_peaks_file",
                "target_input": "geometry_trans_3d_peaks_file",
            }
        ],
    }
    link_6 = {
        "source": "LatFilt",
        "target": "IntFilt",
        "data_mapping": [
            {
                "source_output": "filtered_h5_col_file",
                "target_input": "peaks_3d_file",
            }
        ],
    }
    link_7 = {
        "source": "IntFilt",
        "target": "Indexer",
        "data_mapping": [
            {
                "source_output": "Ifilt_h5_col_file",
                "target_input": "flt_peaks_3d_file",
            }
        ],
    }
    inputs = [
        {"name": "config_dict", "value": inp_config, "id": "init"},
        {"name": "segmenter_config", "value": algo_par, "id": "segFrame"},
        {
            "name": "correction_files",
            "value": inp_config["spline_file"],
            "id": "DetCor",
        },
        {
            "name": "geometry_par_file",
            "value": "/mnt/id03_3dxrd/expt/PROCESSED_DATA/geometry_tdxrd.par",
            "id": "GeoUp",
        },
        {
            "name": "lattice_par_file",
            "value": "/mnt/id03_3dxrd/expt/PROCESSED_DATA/Fe.par",
            "id": "LatFilt",
        },
        {
            "name": "reciprocal_dist_max",
            "value": 1.01,
            "id": "LatFilt",
        },
        {
            "name": "reciprocal_dist_tol",
            "value": 0.01,
            "id": "LatFilt",
        },
        {
            "name": "intensity_frac",
            "value": 0.9837,
            "id": "IntFilt",
        },
        {
            "name": "reciprocal_dist_tol",
            "value": 0.05,
            "id": "Indexer",
        },
        {
            "name": "gen_rings_from_idx",
            "value": (0, 1),
            "id": "Indexer",
        },
        {
            "name": "score_rings_from_idx",
            "value": (0, 1, 2, 3),
            "id": "Indexer",
        },
        {
            "name": "hkl_tols",
            "value": (0.01, 0.02, 0.03, 0.04),
            "id": "Indexer",
        },
        {
            "name": "min_pks_frac",
            "value": (0.9, 0.75),
            "id": "Indexer",
        },
        {
            "name": "cosine_tol",
            "value": np.cos(np.radians(90 - 0.25)),
            "id": "Indexer",
        },
        {
            "name": "max_grains",
            "value": 1000,
            "id": "Indexer",
        },
        {
            "name": "lattice_name",
            "value": "Fe",
            "id": "Indexer",
        },
    ]

    workflow = {
        "graph": {"id": "segment_scan_wf"},
        "nodes": [node_1, node_2, node_3, node_4, node_5, node_6, node_7, node_8],
        "links": [link_1, link_2, link_3, link_4, link_5, link_6, link_7],
    }
    outputs = execute_graph(workflow, inputs=inputs, outputs=[{"all": True}])
    assert outputs is not None
    found_peaks = outputs["found_peaks"]
    assert len(found_peaks[0]) == 76
    detector_spatial_file_path = outputs["detector_spatial_corrected_3d_peaks_file"]
    assert Path(detector_spatial_file_path).exists()
    detector_spatial_corrected_columnfile = PeakColumnFile.columnfile(
        filename=detector_spatial_file_path
    )
    peak_x_position = detector_spatial_corrected_columnfile.f_raw
    assert len(peak_x_position) == 31682
    filtered_h5_col_file = outputs["filtered_h5_col_file"]
    filtered_flt_col_file = outputs["filtered_flt_col_file"]
    lattice_par_file = outputs["copied_lattice_par_file"]

    assert Path(filtered_h5_col_file).exists()
    assert Path(filtered_flt_col_file).exists()
    assert Path(lattice_par_file).exists()

    int_flt_h5_file = outputs["Ifilt_h5_col_file"]
    int_flt_flt_file = outputs["Ifilt_flt_col_file"]
    assert Path(int_flt_h5_file).exists()
    assert Path(int_flt_flt_file).exists()
    cf = PeakColumnFile.columnfile(filename=int_flt_h5_file)
    assert cf.nrows == 7662

    ubi_file_path = outputs["grain_ubi_file"]
    assert Path(ubi_file_path).exists()
    h5_file_path = outputs["grain_nexus_file"]
    assert Path(h5_file_path).exists()
    with h5py.File(h5_file_path, "r") as f:
        assert f["entry/indexed_grains/grains/UBI"][()].shape == (59, 3, 3)
        assert f["entry/indexed_grains/grains/translation"][()].shape == (59, 3)
