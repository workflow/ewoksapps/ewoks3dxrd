import importlib.metadata

project = "ewoks3dxrd"
release = importlib.metadata.version(project)
version = ".".join(release.split(".")[:2])
copyright = "2024-2025, ESRF"
author = "ESRF"

extensions = ["myst_parser", "ewokssphinx", "sphinxcontrib.mermaid"]

myst_enable_extensions = ["colon_fence", "substitution"]
myst_substitutions = {"version": version}
templates_path = ["_templates"]
exclude_patterns = []

always_document_param_types = True


html_theme = "pydata_sphinx_theme"
html_static_path = ["_static"]
html_template_path = ["_templates"]
html_logo = "_static/3dxrd_logo.png"
html_css_files = ["custom.css"]


html_theme_options = {
    "header_links_before_dropdown": 3,
    "navbar_align": "content",
    "show_nav_level": 2,
    "icon_links": [
        {
            "name": "gitlab",
            "url": "https://gitlab.esrf.fr/workflow/ewoksapps/ewoks3dxrd",
            "icon": "fa-brands fa-gitlab",
        },
        {
            "name": "pypi",
            "url": "https://pypi.org/project/ewoks3dxrd",
            "icon": "fa-brands fa-python",
        },
    ],
    "logo": {
        "text": f"{project} {version}",
    },
    "footer_start": ["copyright"],
    "footer_end": ["footer_end"],
}
