from ewokscore import Task
from pathlib import Path
from typing import Tuple

from ImageD11.sinograms.dataset import DataSet as ImageD11_DataSet
from ImageD11.sinograms.dataset import load as load_ImageD11Dataset
import logging

from .models import FolderFileConfig

logger = logging.getLogger(__name__)


class ImageD11PathError(Exception):
    pass


def _extract_ImageD11path_info(path_str: str) -> Tuple[str, str, str, str]:
    """
    Helper function to extract the dataroot, sample, dset_name, scan no from the
    scan folder path
    """
    path = Path(path_str)
    if len(path.parts) < 4:
        raise ImageD11PathError(
            "Expected path structure to be of the for `/dataroot/sample_name/dset_name/scan_number`"
        )

    # check provided path is actually a scan folder
    if len(path_str) < 8 or path_str[-8:-4] != "scan":
        raise ImageD11PathError(
            f"Invalid scan folder path: '{path_str}' should end with `scanxxxx` where xxxx are numbers"
        )

    # Extract scan number (last 4 digits)
    try:
        scan_number = int(path.name[-4:])
    except (ValueError, IndexError):
        raise ImageD11PathError(f"Expected {path.name} to end with 4 numbers.")

    # Extract dset name
    dset_folder_name = path.parent.name
    if "_" not in dset_folder_name:
        raise ImageD11PathError(
            f"Invalid dataset name: no underscore found in {dset_folder_name} to separate sample and dataset name.",
        )

    dset_name = dset_folder_name.split("_")[-1]
    sample_name = path.parent.parent.name
    dataroot = path.parent.parent.parent.__str__()
    return dataroot, sample_name, dset_name, str(scan_number)


class InitFolderFileConfig(
    Task,
    input_names=["config_dict"],
    output_names=["config_dict"],
):
    """Will check paths, motor and detector names and return them as output.

    valid_keys on the config_dict input = [
        "detector",  # str (name): either frelon1 or frelon3 or eiger
        "omega_motor",  # str (name): diffrz
        "dty_motor",  # str (name): diffty
        "bgFile",  # str (path): file_path or None
        "mask_file",  # str (path): file_path or None
        "spline_file",  # str (path): file_path or None
        "e2dx_file",  # str (path): file_path or None
        "e2dy_file",  # str (path): file_path or None
        "scan_folder",  # str (path): existing data scan Bliss path
        "analyse_folder_path",  # str (path): a folder or non existing folder path
        # , it will create here
        "stateful_imageD11_file",  # a imageD11 stateful file for keep the computational state
    ]
    """

    def run(self):
        cfg = FolderFileConfig(**self.inputs.config_dict)

        bliss_folder = Path(cfg.scan_folder)
        dataroot, sample_name, dset_name, scan_number = _extract_ImageD11path_info(
            cfg.scan_folder
        )

        error_message = ""
        has_scan_h5_file = any(
            file.suffix == ".h5" for file in bliss_folder.iterdir() if file.is_file()
        )
        if not has_scan_h5_file:
            error_message += f"Provided ScanFolder: '{cfg.scan_folder}' doesn't have any scan (*.h5) files.\n"

        # parent folder of scan folder should contain a master file, in the name of *.h5 (i.e h5 file)
        dset_folder = bliss_folder.parent
        has_h5_file = any(
            file.suffix == ".h5" for file in dset_folder.iterdir() if file.is_file()
        )

        if not has_h5_file:
            error_message += f"Provided ScanFolder: '{cfg.scan_folder}' doesn't have master file (an .h5 file) in its parent folder.\n"

        analyse_folder = cfg.analyse_folder
        if analyse_folder and not Path(analyse_folder).exists():
            Path(analyse_folder).mkdir(parents=True, exist_ok=True)
            logger.info(f"Provided AnalyseFolder: was created at {analyse_folder}.")

        if not error_message == "":
            raise ImageD11PathError(f"Setup Error: {error_message}\n")

        if (
            cfg.stateful_imageD11_file is None
            or not Path(cfg.stateful_imageD11_file).exists()
        ):
            stateful_ds = ImageD11_DataSet(
                dataroot=dataroot,
                analysisroot=cfg.analyse_folder,
                sample=sample_name,
                dset=dset_name,
                detector=cfg.detector,
                omegamotor=cfg.omega_motor,
                dtymotor=cfg.dty_motor,
            )
        else:
            stateful_ds = load_ImageD11Dataset(h5name=cfg.stateful_imageD11_file)

        stateful_ds.splinefile = cfg.spline_file
        stateful_ds.e2dxfile = cfg.e2dx_file
        stateful_ds.e2dyfile = cfg.e2dy_file
        stateful_ds.maskfile = cfg.mask_file
        stateful_ds.bgfile = cfg.bg_file

        scan_to_import = str(scan_number) + ".1"
        stateful_ds.import_all(scans=[scan_to_import])
        stateful_ds.save(h5name=cfg.stateful_imageD11_file)
        stateful_imageD11_file = stateful_ds.dsfile

        self.outputs.config_dict = {
            **cfg.model_dump(),
            "dataroot": dataroot,
            "sample_name": sample_name,
            "dset_name": dset_name,
            "scan_number": scan_number,
            "stateful_imageD11_file": stateful_imageD11_file,
        }
        logger.info(
            f"Processed intermediate results will be kept in {analyse_folder}/{sample_name}/{sample_name}_{dset_name} folder."
        )  # this folder structure determined by Class Dataset from ImageD11 library
