from ewokscore import TaskWithProgress

from ImageD11 import frelon_peaksearch
from ImageD11 import columnfile as PeakColumnFile

from .models import SegmenterConfig
from .utils import TqdmProgressCallback
from pathlib import Path
import h5py


class SegmentScan(
    TaskWithProgress,
    input_names=["segmenter_config"],
    output_names=["segmented_3d_peaks_file"],
):
    """
    This task segments an entire scan folder,
    merges the peaks, and produces a 3D column file.
    The resulting 3D column peak file is saved.
    Outputs:
        `segmented_3d_peaks_file`: A segmented 3d peaks for the given scan folder is saved in this path.
    """

    def run(self):

        in_seg_config = self.inputs.segmenter_config
        folder_file_config = in_seg_config.get("folder_file_config", None)
        segmenter_parameter = in_seg_config.get("segmenter_parameter", None)

        error_message = []
        if folder_file_config is None:
            error_message.append(
                "'folder_file_config' in 'segmenter_config' is missing. "
                "SegmentScan requires a folder file structure for segmentation."
            )

        if segmenter_parameter is None:
            error_message.append(
                "'segmenter_parameter' in 'segmenter_config' is missing. "
                "SegmentScan requires segmentation parameters."
            )

        if error_message:
            raise ValueError("\n".join(error_message))

        sample_name = folder_file_config.get("sample_name", None)
        if sample_name is None:
            error_message.append(
                "'sample_name' field is missing in 'folder_file_config' of 'segmenter_config'"
                "SegmentScan requires a 'sample_name' string"
            )
        dset_name = folder_file_config.get("dset_name", None)
        if dset_name is None:
            error_message.append(
                "'dset_name' field is missing in 'folder_file_config' of 'segmenter_config'"
                "SegmentScan requires a 'dset_name' string"
            )
        scan_number = folder_file_config.get("scan_number", None)
        if scan_number is None:
            error_message.append(
                "'scan_number' is missing in 'folder_file_config' of 'segmenter_config'"
                "SegmentScan requires a 'scan_number' string"
            )
        detector = folder_file_config.get("detector", None)
        if detector is None:
            error_message.append(
                "'detector' field is missing in 'folder_file_config' of 'segmenter_config'"
                "SegmentScan requires a 'detector' string"
            )
        omega_motor = folder_file_config.get("omega_motor", None)
        if omega_motor is None:
            error_message.append(
                "'omega_motor' field is missing in 'folder_file_config' of 'segmenter_config'"
                "SegmentScan requires a 'omega_motor' string"
            )
        if error_message:
            raise ValueError("\n".join(error_message))

        scan_folder = folder_file_config.get("scan_folder", None)
        masterfile_path = Path(scan_folder).parent / f"{sample_name}_{dset_name}.h5"
        if not masterfile_path.exists():
            raise FileNotFoundError(
                """ 'scan_folder' field in 'folder_file_config' of 'segmenter_config' does not have master file
                        in its parent folder. From its parent folder, we will have its masterfile path.
                    This masterfile path required to compute Omega (rotation) values.
                """
            )
        analyse_folder = folder_file_config.get("analyse_folder", None)
        if analyse_folder is None or not Path(analyse_folder).exists():
            raise FileNotFoundError(
                "Provided 'analyse_folder' field is missing or invalid in 'folder_file_config' of 'segmenter_config'"
            )

        with h5py.File(str(masterfile_path), "r") as hin:
            omega_angles = hin[str(scan_number) + ".1"]["measurement"].get(
                omega_motor, None
            )
            omega_array = omega_angles[()]

        segmenter_cfg = SegmenterConfig(**segmenter_parameter)
        bg_file = folder_file_config.get("bg_file", None)
        mask_file = folder_file_config.get("mask_file", None)
        segmenter_settings = {
            "bgfile": bg_file,
            "maskfile": mask_file,
            "threshold": segmenter_cfg.threshold,
            "smoothsigma": segmenter_cfg.smooth_sigma,
            "bgc": segmenter_cfg.bgc,
            "minpx": segmenter_cfg.min_px,
            "m_offset_thresh": segmenter_cfg.offset_threshold,
            "m_ratio_thresh": segmenter_cfg.ratio_threshold,
        }

        all_frames_2d_peaks_list = frelon_peaksearch.segment_master_file(
            str(masterfile_path),
            str(scan_number) + ".1" + "/measurement/" + detector,
            omega_array,
            segmenter_settings,
            tqdm_class=TqdmProgressCallback,
            TaskInstance=self,
        )
        peaks_2d_dict, num_peaks = frelon_peaksearch.peaks_list_to_dict(
            all_frames_2d_peaks_list
        )
        # 3d merge from 2d peaks dict
        peak_3d_dict = frelon_peaksearch.do3dmerge(
            peaks_2d_dict, num_peaks, omega_array
        )
        segmented_columnfile_3d = PeakColumnFile.colfile_from_dict(peak_3d_dict)
        segmented_3d_columnfile_h5 = f"{analyse_folder}/{sample_name}/{sample_name}_{dset_name}/segmented_3d_peaks.h5"
        PeakColumnFile.colfile_to_hdf(
            segmented_columnfile_3d, segmented_3d_columnfile_h5
        )
        self.outputs.segmented_3d_peaks_file = segmented_3d_columnfile_h5
