import numpy
from pathlib import Path
import os
import pytest

from ewoksdata.tests.data import save_bliss_scan
from ewoksorange.tests.utils import execute_task
from ewoks3dxrd.tasks.initFolderFileConfig import InitFolderFileConfig

OPTIONAL_FIELDS = (
    "bg_file",
    "mask_file",
    "spline_file",
    "e2dx_file",
    "e2dy_file",
)


def test_InitFolderFileConfig(inp_config):

    outputs = execute_task(InitFolderFileConfig, inputs={"config_dict": inp_config})

    assert outputs is not None
    out_config = outputs["config_dict"]
    assert out_config is not None
    assert_matching_fields(out_config, inp_config)
    assert out_config["dataroot"] == "/mnt/id03_3dxrd/expt/RAW_DATA"
    assert out_config["sample_name"] == "FeAu_0p5_tR"
    assert out_config["dset_name"] == "ff1"
    assert out_config["scan_number"] == "1"


def test_InitFolderFileConfig_local(tmp_path):
    # `sample` and `dataset` names are hard-coded in `save_bliss_scan`
    sample = "sample"
    dataset = "dataset"
    scan_number = 1

    dataset_dir = tmp_path / Path(sample) / Path(f"{sample}_{dataset}")
    os.makedirs(dataset_dir, exist_ok=True)

    save_bliss_scan(
        dataset_dir,
        numpy.random.random((800, 600)),
        10,
        50,
        scannr=scan_number,
        lima_names=("frelon3",),
        positioner_names=("diffty", "diffrz"),
        title="scan 1",
    )

    inp_config = {
        "detector": "frelon3",
        "omega_motor": "diffrz",
        "dty_motor": "diffty",
        "scan_folder": str(dataset_dir / "scan0001"),
        "analyse_folder": os.path.join(tmp_path, "test_my_task"),
    }

    outputs = execute_task(InitFolderFileConfig, inputs={"config_dict": inp_config})

    assert outputs is not None
    out_config = outputs["config_dict"]
    assert out_config is not None

    assert_matching_fields(out_config, inp_config)
    assert out_config["dataroot"] == str(tmp_path)
    assert out_config["sample_name"] == sample
    assert out_config["dset_name"] == dataset
    assert out_config["scan_number"] == str(scan_number)


def assert_matching_fields(out_config: dict, inp_config: dict):
    assert out_config["detector"] == inp_config["detector"]
    assert out_config["omega_motor"] == inp_config["omega_motor"]
    assert out_config["dty_motor"] == inp_config["dty_motor"]
    assert out_config["analyse_folder"] == inp_config["analyse_folder"]
    assert Path(out_config["analyse_folder"]).exists()

    for key in OPTIONAL_FIELDS:
        assert out_config.get(key) == inp_config.get(key)

    assert out_config["stateful_imageD11_file"] is not None
    assert Path(out_config["stateful_imageD11_file"]).exists()


def test_wrong_detector(inp_config):
    config_dict = {
        **inp_config,
        "detector": "wrong_detector",
    }

    with pytest.raises(RuntimeError) as exc_info:
        execute_task(InitFolderFileConfig, inputs={"config_dict": config_dict})
    assert "Input should be 'frelon1', 'frelon3' or 'eiger'" in str(
        exc_info.value.__cause__
    )
