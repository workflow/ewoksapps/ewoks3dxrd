from __future__ import annotations

from pathlib import Path
from typing import List, Tuple

import h5py
import numpy as np
from ImageD11 import blobcorrector
from ImageD11 import columnfile as PeakColumnFile
from ImageD11.grain import grain as Grain
from tqdm import tqdm

from .models import UnitCellParameters


class TqdmProgressCallback(tqdm):
    def __init__(self, *args, TaskInstance=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.TaskInstance = TaskInstance
        self.finished_count = 0

    def update(self, n: int = 1):
        super().update(n)
        self.finished_count += n
        if self.TaskInstance:
            self.TaskInstance.progress = 100.0 * (self.finished_count / self.total)


def correct_column_file(filename: str | Path, correction_files: Tuple[str, str] | str):
    segmented_columnfile_3d = PeakColumnFile.columnfile(filename=filename)
    peak_3d_dict = {
        title: segmented_columnfile_3d[title]
        for title in segmented_columnfile_3d.keys()
    }
    peak_3d_dict["spot3d_id"] = np.arange(len(peak_3d_dict["s_raw"]))
    raw_columnfile_3d = PeakColumnFile.colfile_from_dict(peak_3d_dict)

    if isinstance(correction_files, str):
        splinefile = correction_files
        return blobcorrector.correct_cf_with_spline(raw_columnfile_3d, splinefile)

    if isinstance(correction_files, tuple) and len(correction_files) == 2:
        e2dxfile, e2dyfile = correction_files
        return blobcorrector.correct_cf_with_dxdyfiles(
            raw_columnfile_3d, e2dxfile, e2dyfile
        )

    raise ValueError(
        f"Detector Spatial correction cannot be performed unless a spline file or a couple of e2dx, ed2dy files is provided. Got {correction_files}."
    )


def read_lattice_cell_data(lattice_data_file_path: str | Path) -> UnitCellParameters:
    """
    Reads lattice cell data from a file and extracts the lattice parameters and space group.
    """

    read_parameters = {}

    with open(lattice_data_file_path, "r") as file:
        for line in file:
            parts = line.strip().split()
            if len(parts) != 2:
                raise ValueError(f"Invalid line format: {line.strip()}")
            key, value = parts
            read_parameters[key] = value

    return UnitCellParameters(**read_parameters)


def save_grains_to_nexus(
    nexus_filename: str | Path,
    geometry_par_path: str | Path,
    lattice_par_file: str | Path,
    lattice_name: str,
    grains: List[Grain],
) -> None:
    """
    Save grain information (UBI matrices, lattice parameters) and
    the experiment geometry into a NeXus file.

    Parameters:
    - nexus_filename: Output NeXus file path
    - geometry_par_path: Path to the geometry_tdxrd.par file
    - lattice_par_file: Path to lattice parameters file
    - lattice_name: Name of the lattice type
    - grains (list of grain instances): List of grain objects with:
        - grain.ubi: (3,3) array
    """

    unit_cell_parameters = read_lattice_cell_data(
        lattice_data_file_path=lattice_par_file
    )
    lattice_parameters, space_group = (
        unit_cell_parameters.lattice_parameters,
        unit_cell_parameters.space_group,
    )

    with open(geometry_par_path, "r") as f:
        geometry_lines = [line.strip() for line in f]
    geometry_par = "\n".join(geometry_lines)

    num_grains = len(grains)
    ubi_matrices = np.zeros((num_grains, 3, 3), dtype=grains[0].ubi.dtype)
    translations = np.full((num_grains, 3), fill_value=np.nan)

    for i, grain in enumerate(grains):
        ubi_matrices[i] = grain.ubi
        if grain.translation:
            translations[i] = grain.translation

    with h5py.File(nexus_filename, "w") as f:
        entry = f.create_group("entry")
        entry.attrs["NX_class"] = "NXentry"
        index_grains = entry.create_group("indexed_grains")
        index_grains.attrs["NX_class"] = "NXprocess"
        parameters_group = index_grains.create_group("parameters")
        parameters_group.attrs["NX_class"] = "NXcollection"
        parameters_group.create_dataset("geometry_par", data=geometry_par)
        parameters_group.create_dataset("lattice_name", data=lattice_name)
        parameters_group.create_dataset(
            "lattice_parameters", data=np.array(lattice_parameters)
        )
        if isinstance(space_group, int):
            parameters_group.create_dataset("space_group_number", data=space_group)
        else:
            parameters_group.create_dataset("space_group_symbol", data=space_group)

        grains_group = index_grains.create_group("grains")
        grains_group.attrs["NX_class"] = "NXdata"
        grains_group.create_dataset("UBI", data=ubi_matrices)
        grains_group.create_dataset("translation", data=translations)


def read_wavelength(geometry_par_file: str | Path) -> float:
    with open(geometry_par_file, "r") as f:
        for line in f:
            if line.startswith("wavelength"):
                try:
                    wavelength = float(line.strip().split()[1])
                    break
                except (IndexError, ValueError):
                    raise ValueError(
                        f"Could not found a valid wavelength field in {geometry_par_file}"
                    )
    return wavelength
