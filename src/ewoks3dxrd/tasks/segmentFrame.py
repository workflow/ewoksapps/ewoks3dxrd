from ewokscore import Task
from pathlib import Path

from ImageD11.nbGui import segmenter_gui_utils
from ImageD11 import frelon_peaksearch
from ImageD11.sinograms.dataset import DataSet as ImageD11DataSet

from .models import SegmenterConfig


class SegmentFrame(
    Task,
    input_names=["folder_file_config", "segmenter_config"],
    output_names=[
        "merged_config",
        "raw_image",
        "bg_corrected_image",
        "found_peaks",
    ],
):
    """
    This task produces single segmented frame
    Also, it merges two input configs 'segmenter_config' and 'folder_file_config'

    Example values for `parameter_config`:
        ref: https://github.com/FABLE-3DXRD/ImageD11/blob/master/ImageD11/frelon_peaksearch.py
    {
        # Zeroing Below Intensity, type: int
        "threshold": 50,
        # Gaussian Filter Sigma, type: float
        "smooth_sigma": 1.0,
        # Remove Bg per peak, type: float
        "bgc": 0.9,
        # Required Min Pixels for a peak, type: int
        "min_px": 3,
        # Constantization if less than equal to, type: int
        "offset_threshold": 100,
        # Normalization within the range from greater than equal to upto
        # the maximum value from the raw data, type: int
        "ratio_threshold": 135
    }
    """

    def run(self):

        folder_file_config = self.inputs.folder_file_config
        par_config = self.inputs.segmenter_config

        config_error_message = ""
        if folder_file_config is None:
            config_error_message += """
                            In the 'SegmentFrame' Task:
                            \t File Folder Configuration is None
                            """

        if par_config is None:
            config_error_message += """
                            In the 'SegmentFrame' Task:
                            \t Frelon Segmenter Parameter is None
                            """

        if not config_error_message == "":
            raise ValueError(config_error_message)

        segmenter_cfg = SegmenterConfig(**par_config)

        bg_file = folder_file_config.get("bg_file", None)
        mask_file = folder_file_config.get("mask_file", None)
        dset_filename = folder_file_config.get("stateful_imageD11_file", None)

        if dset_filename is None or not Path(dset_filename).exists():
            raise FileNotFoundError(
                f"Provided dset_file {dset_filename} does not exist"
            )

        dset = ImageD11DataSet(filename=dset_filename)
        _, _, raw_image = segmenter_gui_utils.chooseframe(
            dset=dset, fetch_raw_image=True
        )

        # Adapted from https://github.com/FABLE-3DXRD/ImageD11/blob/6da68dc76c2427fe418d15d23eb4769be0a3b4b5/ImageD11/nbGui/segmenter_gui.py#L141
        raw_image = raw_image.astype("uint16")
        image_worker = frelon_peaksearch.worker(
            bgfile=bg_file,
            maskfile=mask_file,
            threshold=segmenter_cfg.threshold,
            smoothsigma=segmenter_cfg.smooth_sigma,
            bgc=segmenter_cfg.bgc,
            minpx=segmenter_cfg.min_px,
            m_offset_thresh=segmenter_cfg.offset_threshold,
            m_ratio_thresh=segmenter_cfg.ratio_threshold,
        )
        goodpeaks = image_worker.peaksearch(img=raw_image, omega=0)
        # 23 and 24 are the columns for fast_column index and slow column index
        peak_positions = goodpeaks[:, 23:25].T

        self.outputs.raw_image = raw_image
        self.outputs.bg_corrected_image = image_worker.smoothed
        self.outputs.found_peaks = peak_positions
        self.outputs.merged_config = {
            "folder_file_config": folder_file_config,
            "segmenter_parameter": segmenter_cfg.model_dump(),
        }
